function thuNhapChiuThue(thue) {
  if (thue > 0 && thue <= 60e6) {
    tienThue = thue * 0.05;
  } else if (thue > 60e6 && thue <= 120e6) {
    tienThue = thue * 0.1;
  } else if (thue > 120e6 && thue <= 210e6) {
    tienThue = thue * 0.15;
  } else if (thue > 210e6 && thue <= 384e6) {
    tienThue = thue * 0.2;
  } else if (thue > 384e6 && thue <= 624e6) {
    tienThue = thue * 0.25;
  } else if (thue > 624e6 && thue <= 960e6) {
    tienThue = thue * 0.3;
  } else {
    tienThue = thue * 0.35;
  }
  return tienThue;
}

function tinhThue() {
  const hoTen = document.getElementById("ho-ten").value;
  let thuNhap = document.getElementById("thu-nhap").value * 1;
  let soNguoi = document.getElementById("so-nguoi").value * 1;

  thue = thuNhap - 4000000 - soNguoi * 1600000;
  let soTienChiuThue = thuNhapChiuThue(thue);
  soTienChiuThue = soTienChiuThue.toLocaleString();
  document.getElementById(
    `result`
  ).innerHTML = `Họ tên: ${hoTen}; Tiền thuế thu nhập cá nhân: ${soTienChiuThue} VND`;
}
