function showKetNoi() {
  const customer = document.getElementById("select").value;
  if (customer == "DN") {
    document.getElementById(`ket-noi`).style.display = `block`;
  } else {
    document.getElementById(`ket-noi`).style.display = `none`;
  }
}

function tinhTienCapNhaDan(kenhCC) {
  tongTien = 7.5 * kenhCC + 4.5 + 20.5;
  return tongTien;
}

function tinhTienCapDoanhNghiep(kenhCC, kenhKN) {
  if (kenhKN <= 10) {
    tongTien = 50 * kenhCC + 75 + 15;
    return tongTien;
  } else {
    tongTien = 50 * kenhCC + 75 + 15 + (kenhKN - 10) * 5;
    return tongTien;
  }
}

function tinhTienCap() {
  const customer = document.getElementById("select").value;
  const maKH = document.getElementById("customer-id").value;
  var kenhCC = document.getElementById("so-kenh-cao-cap").value * 1;
  var kenhKN = document.getElementById(`so-kenh-ket-noi`).value * 1;
  if (customer == "ND") {
    tienCap = tinhTienCapNhaDan(kenhCC);
    var tienCap = new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
    }).format(tienCap);
    document.getElementById(
      `result`
    ).innerHTML = `Mã khách hàng: ${maKH}; Tiền cáp ${tienCap}`;
  } else {
    tienCap = tinhTienCapDoanhNghiep(kenhCC, kenhKN);
    var tienCap = new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
    }).format(tienCap);
    document.getElementById(
      `result`
    ).innerHTML = `Mã khách hàng: ${maKH} ; Tiền cáp ${tienCap}`;
  }
}
