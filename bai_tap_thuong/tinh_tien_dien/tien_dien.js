function tinhTienDien() {
  const hoTen = document.getElementById("ho-ten").value;
  var tongSoKW = document.getElementById("soKW").value * 1;
  const muc1 = 500,
    muc2 = 650,
    muc3 = 850,
    muc4 = 1100,
    muc5 = 1300;

  if (tongSoKW >= 0 && tongSoKW <= 50) {
    tongTien = tongSoKW * muc1;
  } else if (tongSoKW > 50 && tongSoKW <= 100) {
    tongTien = 50 * muc1 + (tongSoKW - 50) * muc2;
  } else if (tongSoKW > 100 && tongSoKW <= 200) {
    tongTien = 50 * muc1 + 50 * muc2 + (tongSoKW - 100) * muc3;
  } else if (tongSoKW > 200 && tongSoKW <= 350) {
    tongTien = 50 * muc1 + 50 * muc2 + 100 * muc3 + (tongSoKW - 200) * muc4;
  } else {
    tongTien =
      50 * muc1 + 50 * muc2 + 100 * muc3 + 150 * muc4 + (tongSoKW - 350) * muc5;
  }
  tongTien = tongTien.toLocaleString();
  document.getElementById(
    `result`
  ).innerHTML = `Họ tên: ${hoTen}; Tiền điện: ${tongTien}`;
}
