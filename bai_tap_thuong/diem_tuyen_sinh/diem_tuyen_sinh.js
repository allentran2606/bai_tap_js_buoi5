function tinhDiemTong() {
  const diem1 = document.getElementById("diem-mon-1").value * 1;
  const diem2 = document.getElementById("diem-mon-2").value * 1;
  const diem3 = document.getElementById("diem-mon-3").value * 1;
  var tongDiem = 0;
  if (diem1 == 0 || diem2 == 0 || diem3 == 0) {
  } else {
    tongDiem = diem1 + diem2 + diem3;
  }
  return tongDiem;
}

function tinhDiemKhuVuc() {
  const khuVuc = document.getElementById("khu-vuc").value;
  if (khuVuc == `A`) {
    diemKhuVuc = 2;
  } else if (khuVuc == `B`) {
    diemKhuVuc = 1;
  } else if (khuVuc == `C`) {
    diemKhuVuc = 0.5;
  } else {
    diemKhuVuc = 0;
  }
  return diemKhuVuc;
}
function tinhDiemDoiTuong() {
  const doiTuong = document.getElementById("doi-tuong").value;
  if (doiTuong == 1) {
    diemDoiTuong = 2.5;
  } else if (doiTuong == 2) {
    diemDoiTuong = 1.5;
  } else if (doiTuong == 3) {
    diemDoiTuong = 1;
  } else {
    diemDoiTuong = 0;
  }
  return diemDoiTuong;
}
function xetDiem() {
  const diemChuan = document.getElementById("diem-chuan").value * 1;
  const doiTuong = document.getElementById("doi-tuong").value;
  const khuVuc = document.getElementById("khu-vuc").value;
  diemTongKet = tinhDiemTong() + tinhDiemKhuVuc() + tinhDiemDoiTuong();
  if (diemTongKet >= diemChuan) {
    document.getElementById(
      `result`
    ).innerHTML = `Bạn đã đậu. Tổng điểm của bạn là: ${diemTongKet}`;
  } else {
    document.getElementById(
      `result`
    ).innerHTML = `Bạn đã rớt. Tổng điểm của bạn là: ${diemTongKet}`;
  }
}
